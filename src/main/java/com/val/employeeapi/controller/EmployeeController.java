package com.val.employeeapi.controller;

import com.val.employeeapi.domain.Employee;
import com.val.employeeapi.service.EmployeeService;
import com.val.employeeapi.service.SupervisorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("employees")
@CrossOrigin(origins = "*")
public class EmployeeController {

    private EmployeeService employeeService;
    private SupervisorService supervisorService;

    @Autowired
    public EmployeeController(EmployeeService employeeService, SupervisorService supervisorService) {
        this.employeeService = employeeService;
        this.supervisorService = supervisorService;
    }
    @GetMapping
    public List<Employee> viewAllEmployees(){
        return employeeService.viewAllEmployees();
    }

    @PostMapping
    public Employee addEmployee(@RequestBody Employee employee){
        System.out.println(employee);
        return employeeService.addEmployee(employee);
    }

    @PutMapping
    public Employee editEmployee(@RequestParam Long id,@RequestBody Employee employee){
        return employeeService.editEmployee(id,employee);
    }

    @DeleteMapping
    public void deleteEmployee(@RequestParam Long id){
        employeeService.deleteEmployee(id);
    }

    @GetMapping("view")
    public Employee getEmployeeById(@RequestParam long id){
        return employeeService.getEmployeeById(id);
    }

    @GetMapping("sId")
    public List<Employee> getEmployeeBySupervisorId(@RequestParam Long id){
        return employeeService.getEmployeeBySupervisorId(id);
    }

}


