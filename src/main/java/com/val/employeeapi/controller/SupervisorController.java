package com.val.employeeapi.controller;

import com.val.employeeapi.domain.Supervisor;
import com.val.employeeapi.service.EmployeeService;
import com.val.employeeapi.service.SupervisorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;


@RestController
@RequestMapping("supervisors")
@CrossOrigin(origins = "*")
public class SupervisorController {

    private SupervisorService supervisorService;
    private EmployeeService employeeService;

    @Autowired
    public SupervisorController(SupervisorService supervisorService, EmployeeService employeeService) {
        this.supervisorService = supervisorService;
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<Supervisor> viewAllSupervisors(){
        return supervisorService.viewAllSupervisors();
    }

    @PostMapping
    public Supervisor addSupervisor(@RequestBody Supervisor supervisor){
        return supervisorService.addSupervisor(supervisor);
    }

    @PutMapping
    public Supervisor editSupervisor(@RequestParam Long idSuper,@RequestBody Supervisor supervisor){
        return supervisorService.editSupervisor(idSuper,supervisor);
    }

    @DeleteMapping
    public void deleteSupervisor(@RequestParam Long idSuper){
        supervisorService.deleteSupervisor(idSuper);
    }

    @GetMapping("view")
    public Supervisor getSupervisorByIdSuper(Long idSuper){
        return supervisorService.getSupervisorByIdSuper(idSuper);
    }
}
