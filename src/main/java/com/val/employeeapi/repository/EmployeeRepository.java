package com.val.employeeapi.repository;

import com.val.employeeapi.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface EmployeeRepository extends JpaRepository <Employee, Long> {
    Employee getEmployeeById(Long id);
    List<Employee> getEmployeesBySupervisorIdSuper(Long id);
}
