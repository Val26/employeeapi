package com.val.employeeapi.repository;

import com.val.employeeapi.domain.Employee;
import com.val.employeeapi.domain.Supervisor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupervisorRepository extends JpaRepository <Supervisor, Long> {
    Supervisor getSupervisorByIdSuper(Long idSuper);
}
