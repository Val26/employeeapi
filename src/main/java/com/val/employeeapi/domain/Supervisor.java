package com.val.employeeapi.domain;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Supervisor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSuper;
    private String firstNameSuper;
    private String lastNameSuper;
    private String gender;

    @OneToMany(mappedBy = "supervisor")
    private List<Employee> employeeList;

    public Supervisor() {
    }

    public Supervisor(Long idSuper, String firstNameSuper, String lastNameSuper, String gender, List<Employee> employeeList) {
        this.idSuper = idSuper;
        this.firstNameSuper = firstNameSuper;
        this.lastNameSuper = lastNameSuper;
        this.gender = gender;
        this.employeeList = employeeList;
    }

    public Long getIdSuper() {
        return idSuper;
    }

    public void setIdSuper(Long idSuper) {
        this.idSuper = idSuper;
    }

    public String getFirstNameSuper() {
        return firstNameSuper;
    }

    public void setFirstNameSuper(String firstNameSuper) {
        this.firstNameSuper = firstNameSuper;
    }

    public String getLastNameSuper() {
        return lastNameSuper;
    }

    public void setLastNameSuper(String lastNameSuper) {
        this.lastNameSuper = lastNameSuper;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Supervisor that = (Supervisor) o;
        return Objects.equals(idSuper, that.idSuper);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSuper);
    }

    @Override
    public String toString() {
        return "Supervisor{" +
                "idSuper=" + idSuper +
                ", firstNameSuper='" + firstNameSuper + '\'' +
                ", lastNameSuper='" + lastNameSuper + '\'' +
                ", gender='" + gender + '\'' +
                ", employeeList=" + employeeList +
                '}';
    }
}
