package com.val.employeeapi.service;

import com.val.employeeapi.domain.Employee;
import com.val.employeeapi.domain.Supervisor;
import com.val.employeeapi.repository.EmployeeRepository;
import com.val.employeeapi.repository.SupervisorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SupervisorService {

    private EmployeeRepository employeeRepository;
    private SupervisorRepository supervisorRepository;

    @Autowired
    public SupervisorService(SupervisorRepository supervisorRepository){
        this.supervisorRepository = supervisorRepository;
    }

    public List<Supervisor> viewAllSupervisors(){
        return supervisorRepository.findAll();

    }
    public Supervisor addSupervisor(Supervisor supervisor){
        return supervisorRepository.save(supervisor);
    }
    public Supervisor editSupervisor(Long idSuper, Supervisor supervisor){
        List<Supervisor> supervisorList = viewAllSupervisors();
        supervisor.setIdSuper(idSuper);
        return supervisorRepository.save(supervisor);
    }
    public void deleteSupervisor(Long idSuper){
        System.out.println(idSuper);
        supervisorRepository.deleteById(idSuper);
    }

    public Supervisor getSupervisorByIdSuper(Long idSuper){
        return supervisorRepository.getSupervisorByIdSuper(idSuper);
    }


}
