package com.val.employeeapi.service;

import com.val.employeeapi.domain.Employee;
import com.val.employeeapi.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> viewAllEmployees(){
        return employeeRepository.findAll();

    }
    public Employee addEmployee(Employee employee){
        employeeRepository.save(employee);
        return employee;

    }
    public Employee editEmployee(Long id,Employee employee){
        List<Employee>employeeList = viewAllEmployees();
        employee.setId(id);
        return employeeRepository.save(employee);
    }
    public void deleteEmployee(Long id){
        List<Employee>employeeList = viewAllEmployees();
        employeeRepository.deleteById(id);
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.getEmployeeById(id);

    }

    public List<Employee> getEmployeeBySupervisorId(Long idSuper){
        return employeeRepository.getEmployeesBySupervisorIdSuper(idSuper);
    }


}
